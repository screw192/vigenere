const express = require("express");
const router = express.Router();

const Vigenere = require('caesar-salad').Vigenere;

router.post("/", (req, res) => {
  const {message, password} = req.body;
  const encryptedMessage = Vigenere.Cipher(password).crypt(message);

  res.send({message: encryptedMessage});
});

module.exports = router;