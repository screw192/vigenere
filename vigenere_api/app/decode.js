const express = require("express");
const router = express.Router();

const Vigenere = require('caesar-salad').Vigenere;

router.post("/", (req, res) => {
  const {message, password} = req.body;
  const decryptedMessage = Vigenere.Decipher(password).crypt(message);

  res.send({message: decryptedMessage});
});

module.exports = router;