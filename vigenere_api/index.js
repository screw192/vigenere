const express = require("express");
const cors = require("cors");
const encode = require("./app/encode");
const decode = require("./app/decode");

const app = express();
app.use(express.json());
app.use(cors());


const port = 8000;

app.use("/encode", encode);
app.use("/decode", decode);

app.listen(port, () => {
  console.log("Listening on port: " + port);
});