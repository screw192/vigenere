import {
  DECRYPT_MESSAGE_FAILURE,
  DECRYPT_MESSAGE_REQUEST,
  DECRYPT_MESSAGE_SUCCESS,
  ENCRYPT_MESSAGE_FAILURE,
  ENCRYPT_MESSAGE_REQUEST,
  ENCRYPT_MESSAGE_SUCCESS, INPUT_CHANGE
} from "../actions/vigenereActions";

const initialState = {
  decrypted: "",
  encrypted: "",
  encryptionPassword: "",
};

const vigenereReducer =  (state = initialState, action) => {
  switch (action.type) {
    case INPUT_CHANGE:
      return {...state, [action.name]: action.value};
    case ENCRYPT_MESSAGE_REQUEST:
      return {...state};
    case ENCRYPT_MESSAGE_SUCCESS:
      return {...state, encrypted: action.encryptedMessage.message};
    case ENCRYPT_MESSAGE_FAILURE:
      return {...state};
    case DECRYPT_MESSAGE_REQUEST:
      return {...state};
    case DECRYPT_MESSAGE_SUCCESS:
      return {...state, decrypted: action.decryptedMessage.message};
    case DECRYPT_MESSAGE_FAILURE:
      return {...state};
    default:
      return state;
  }
};

export default vigenereReducer;