import axiosVigenere from "../../axiosVigenere";

export const INPUT_CHANGE = "INPUT_CHANGE";

export const inputChange = (name, value) => ({type: INPUT_CHANGE, name, value});

export const ENCRYPT_MESSAGE_REQUEST = "ENCRYPT_MESSAGE_REQUEST";
export const ENCRYPT_MESSAGE_SUCCESS = "ENCRYPT_MESSAGE_SUCCESS";
export const ENCRYPT_MESSAGE_FAILURE = "ENCRYPT_MESSAGE_FAILURE";

export const encryptMessageRequest = () => ({type: ENCRYPT_MESSAGE_REQUEST});
export const encryptMessageSuccess = encryptedMessage => ({type: ENCRYPT_MESSAGE_SUCCESS, encryptedMessage});
export const encryptMessageFailure = () => ({type: ENCRYPT_MESSAGE_FAILURE});

export const encryptMessage = (message, password) => {
  return async dispatch => {
    try {
      dispatch(encryptMessageRequest());
      const response = await axiosVigenere.post("/encode", {message: message, password: password});
      dispatch(encryptMessageSuccess(response.data));
    } catch (e) {
      dispatch(encryptMessageFailure());
    }
  };
};

export const DECRYPT_MESSAGE_REQUEST = "DECRYPT_MESSAGE_REQUEST";
export const DECRYPT_MESSAGE_SUCCESS = "DECRYPT_MESSAGE_SUCCESS";
export const DECRYPT_MESSAGE_FAILURE = "DECRYPT_MESSAGE_FAILURE";

export const decryptMessageRequest = () => ({type: DECRYPT_MESSAGE_REQUEST});
export const decryptMessageSuccess = decryptedMessage => ({type: DECRYPT_MESSAGE_SUCCESS, decryptedMessage});
export const decryptMessageFailure = () => ({type: DECRYPT_MESSAGE_FAILURE});

export const decryptMessage = (message, password) => {
  return async dispatch => {
    try {
      dispatch(decryptMessageRequest());
      const response = await axiosVigenere.post("/decode", {message: message, password: password});
      dispatch(decryptMessageSuccess(response.data));
    } catch (e) {
      dispatch(decryptMessageFailure());
    }
  };
};