import React from "react";
import {useDispatch, useSelector} from "react-redux";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import TextField from '@material-ui/core/TextField';
import IconButton from "@material-ui/core/IconButton";
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

import {decryptMessage, encryptMessage, inputChange} from "./store/actions/vigenereActions";

const useStyles = makeStyles(theme => ({
  app: {
    marginTop: "20px"
  },
  textareaLoading: {
    height: "113px",
    border: "1px solid rgba(0, 0, 0, 0.23)",
    borderRadius: "4px"
  },
  buttons: {
    marginLeft: "10px"
  }
}));


const App = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const vigenere = useSelector(state => state.vigenere);

  return (
    <div className="App">
      <Grid container direction="column" spacing={3} className={classes.app}>
        <Grid item xs sm md={6} lg={4}>
          <TextField
              name="decrypted"
              id="outlined-multiline-static"
              label="Decrypted:"
              multiline
              fullWidth
              rows={4}
              value={vigenere.decrypted}
              onChange={
                (e) => dispatch(inputChange(e.target.name, e.target.value))
              }
              variant="outlined"
          />
        </Grid>
        <Grid item container xs sm md={6} lg={4} alignItems="center">
          <Box flexGrow={1}>
            <TextField
                name="encryptionPassword"
                id="outlined-basic"
                label="Password:"
                fullWidth
                value={vigenere.encryptionPassword}
                onChange={
                  (e) => dispatch(inputChange(e.target.name, e.target.value))
                }
                variant="outlined"
            />
          </Box>
          <IconButton
              className={classes.buttons}
              onClick={() => dispatch(decryptMessage(vigenere.encrypted, vigenere.encryptionPassword))}
          >
            <ArrowUpwardIcon />
          </IconButton>
          <IconButton
              className={classes.buttons}
              onClick={() => dispatch(encryptMessage(vigenere.decrypted, vigenere.encryptionPassword))}
          >
            <ArrowDownwardIcon />
          </IconButton>
        </Grid>
        <Grid item xs sm md={6} lg={4}>
          <TextField
              name="encrypted"
              id="outlined-multiline-static"
              label="Encrypted:"
              multiline
              fullWidth
              rows={4}
              value={vigenere.encrypted}
              onChange={
                (e) => dispatch(inputChange(e.target.name, e.target.value))
              }
              variant="outlined"
          />
        </Grid>
      </Grid>
    </div>
  );
}

export default App;
